// Instruction 3
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET',
})
.then((response) => response.json())
.then((data) => console.log(data))

// Instruction 4 .map method

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET',
})
.then((response) => response.json())
.then((data) => {
	console.log(data.map(toDo => {
		return toDo.title
	}))
})


// Instruction 5
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET',
})
.then((response) => response.json())
.then((data) => console.log(data))

// Instruction 6
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET',
})
.then((response) => response.json())
.then((data) => {
	console.log(data, (title, completed) => {
		return `This item ${title} on the list has a status ${completed}`
	})
})

// Instruction 7

fetch('https:jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'Created to do list',
		completed: false
	})
})
.then((response) => response.json())
.then((data) => console.log(data)) 


// Instruction 8
fetch('https:jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated to do list',
	})
})
.then((response) => response.json())
.then((data) => console.log(data)) 

// Instruction 9
let today = new Date();

fetch('https:jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		descpription: 'To update my to do list with a different data structure',
		status: "Pending",
		dateCompleted: today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
	})
})
.then((response) => response.json())
.then((data) => console.log(data)) 


// Instruction 10
today = new Date();

fetch('https:jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		descpription: 'To update my to do list with a different data structure',
		status: "Pending",
		dateCompleted: today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
	})
})
.then((response) => response.json())
.then((data) => console.log(data)) 

// Instruction 12

fetch('https:jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))

